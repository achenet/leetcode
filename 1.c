// https://leetcode.com/problems/two-sum/description/ 
// Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

// You may assume that each input would have exactly one solution, and you may not use the same element twice.

// You can return the answer in any order.
#include <stdio.h>


// O(n) type algo
// int* twoSum(int* nums, int numsSize, int target, int* returnSize) {
// 	int ans[2] = {0, 1};
// 	for (int i = 0; i < numsSize; i++)
// 		for (int j = i+1; j < numsSize; j++)
// 			if (nums[i] + nums[j] == target)
// 			{
// 				// printf("i: %d,j: %d, nums[i]: %d, nums[j]: %d\n", i, j, nums[i], nums[j]);
// 				ans[0] = i;
// 				ans[1] = j;
// 			}
// 		return ans;
// }
// fails, returns [] instead of [0,1]
// note: returned array must be malloced, assume caller calls free


int* twoSum(int* nums, int numsSize, int target, int* returnSize) {
	int* ans = malloc(2*sizeof(int));
	for (int i = 0; i < numsSize; i++)
		for (int j = i+1; j < numsSize; j++)
			if (nums[i] + nums[j] == target)
			{
				// printf("i: %d,j: %d, nums[i]: %d, nums[j]: %d\n", i, j, nums[i], nums[j]);
				ans[0] = i;
				ans[1] = j;
			}
		return ans;
}


char* prettyPrint(int* ans) {
	char* toto = "toto";
	char* out;
	if (0 > asprintf(&out, "[%d, %d]", *ans, *(ans+1))) {
		return "error";
	}
	return out;
}

int main() {
	printf("testing twoSum:\n");
	int nums[4] = {2, 7, 11, 15};
	int numsSize = 4;
	int target = 9;
	int* returnSize;
	int* ans = twoSum(nums, numsSize, target, returnSize);
	char* out = prettyPrint(ans);
	printf("%s\n", out);
	return 0;
}


